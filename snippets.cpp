/*
 * Code snippets for later use
 */


/*
 * MIDI
 * Within the loop set the inital timestamp
 * calculate tomes to set noteOn and noteOff midi stuff
 */
unsigned long currentMillis = millis();
if (currentMillis - previousMillis >= seqData.tempo / 2) {
  int step = seqData.currentStep > 0 ? seqData.currentStep - 1 : 7;
  if (seqData.noteOn[step]) {
    usbMIDI.sendNoteOff(
      seqData.encoders.note[step],
      0,
      1
    );
    seqData.noteOn[step] = false;
  }
}

if (currentMillis - previousMillis >= seqData.tempo) {
  previousMillis = currentMillis;
  usbMIDI.sendNoteOn(
    seqData.encoders.note[seqData.currentStep],
    seqData.encoders.velocity[seqData.currentStep],
    1
  );
  seqData.noteOn[seqData.currentStep] = true;

  if (seqData.currentStep == 7) {
    seqData.currentStep = 0;
  } else {
    seqData.currentStep++;
  }
}

