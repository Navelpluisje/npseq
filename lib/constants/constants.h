// app.h
#ifndef NPSEQ_APP_CONST
  #define NPSEQ_APP_CONST

  #include <Arduino.h>

  /*
   * Colors
   */
  #define NPSEQ_RED 0xDA68
  #define NPSEQ_DARKRED 0xC228
  #define NPSEQ_BLACK 0x4208
  #define NPSEQ_GRAY 0xB596

  /**
   * Expanders
   */
  #define NPSEQ_ENC_EXPANDER 0
  #define NPSEQ_CONTROL_EXPANDER 1
  #define NPSEQ_OUTPUT_EXPANDER 2

  struct IOExpander {
    int8_t address;
    int8_t interrupt;
    bool interrupted;
    int8_t portALast;
    byte type;
    void (*callback)();
  };

  /**
   * Menu
   */
  struct MenuItem {
    bool hasChildren;
    char* name;
  };

  struct Menu {
    MenuItem items[];
  };

  const byte MIDI_PORTS[] = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16};
  const byte SEQ_MIN_PORT = 0;
  const byte SEQ_MAX_PORT = 16;
  const byte SEQ_MIN_STEPS = 4;
  const byte SEQ_MAX_STEPS = 16;
  const byte SEQ_MIN_TEMPO = 20;
  const byte SEQ_MAX_TEMPO = 240;

  const MenuItem seqMenu[] = {
    {
      false,
      "Menu 2", // dummy menu item
    }, {
      false,
      "Steps",
    }, {
      false,
      "MIDI Port",
    }, {
      false,
      "About",
    }, {
      false,
      "Close Menu",
    }
  };
  const int nbMenuItems = sizeof(seqMenu) / sizeof(*seqMenu);

  /*
   * Ports
   */
  const int8_t sclk = 13;  // SPI Clock
  const int8_t miso = 12;  // Master In Slave Out
  const int8_t mosi = 11;  // Master In Slave Out

  const int8_t csTFT = 9;  // Chip/Slave Select for the TFT display
  const int8_t clkTFT = 5; // TFT specific CLK
  const int8_t mosiTFT = 6; // TFT specific MOSI
  const int8_t dcTFT = 7;  // but certain pairs must NOT be used: 2+10, 6+9, 20+23, 21+22
  const int8_t rstTFT = 8; // Reset Pin for TFT Display

  const int8_t csEnc = 10; // Chip/Slave Select for the I/O encoder expanders
  const int8_t rstEnc = 7; // Rest pin for the I/O encoder expanders

  // Set the interrupt ports for the IO-expanders
  const int8_t intEnc0 = 18;
  const int8_t intEnc1 = 19;
  const int8_t intEnc2 = 20;
  const int8_t intEnc3 = 21;
  const int8_t intEnc4 = 22;

  const char notesList[128][4] = {
    "C", "C#", "D", "D#", "E", "F", "F#", "G", "G#", "A", "A#", "B",
    "C0", "C#0", "D0", "D#0", "E0", "F0", "F#0", "G0", "G#0", "A0", "A#0", "B0",
    "C1", "C#1", "D1", "D#1", "E1", "F1", "F#1", "G1", "G#1", "A1", "A#1", "B1",
    "C2", "C#2", "D2", "D#2", "E2", "F2", "F#2", "G2", "G#2", "A2", "A#2", "B2",
    "C3", "C#3", "D3", "D#3", "E3", "F3", "F#3", "G3", "G#3", "A3", "A#3", "B3",
    "C4", "C#4", "D4", "D#4", "E4", "F4", "F#4", "G4", "G#4", "A4", "A#4", "B4",
    "C5", "C#5", "D5", "D#5", "E5", "F5", "F#5", "G5", "G#5", "A5", "A#5", "B5",
    "C6", "C#6", "D6", "D#6", "E6", "F6", "F#6", "G6", "G#6", "A6", "A#6", "B6",
    "C7", "C#7", "D7", "D#7", "E7", "F7", "F#7", "G7", "G#7", "A7", "A#7", "B7",
    "C8", "C#8", "D8", "D#8", "E8", "F8", "F#8", "G8", "G#8", "A8", "A#8", "B8",
    "C9", "C#9", "D9", "D#9", "E9", "F9", "F#9", "G9",
  };

  /*
   * Colors
   */
  const int image[][6][3] = {
    {{6, 13, NPSEQ_RED}, {23, 49, NPSEQ_RED}},
    {{0, 14, NPSEQ_RED},{23, 49, NPSEQ_RED}},
    {{0, 14, NPSEQ_RED},{22, 50, NPSEQ_RED}},
    {{0, 15, NPSEQ_RED},{22, 50, NPSEQ_RED}},
    {{0, 15, NPSEQ_RED},{22, 50, NPSEQ_RED}},
    {{0, 51, NPSEQ_RED}},
    {{0, 51, NPSEQ_RED}},
    {{0, 16, NPSEQ_RED},{21, 52, NPSEQ_RED}},
    {{0, 16, NPSEQ_RED},{21, 52, NPSEQ_RED}},
    {{0, 16, NPSEQ_RED},{21, 53, NPSEQ_RED}}, // 10

    {{0, 16, NPSEQ_RED},{21, 53, NPSEQ_RED}},
    {{0, 16, NPSEQ_RED},{22, 54, NPSEQ_RED}},
    {{0, 15, NPSEQ_RED},{22, 51, NPSEQ_RED},{53, 55, NPSEQ_RED}},
    {{0, 15, NPSEQ_RED},{23, 51, NPSEQ_RED},{54, 55, NPSEQ_RED}},
    {{0, 14, NPSEQ_RED},{23, 50, NPSEQ_RED},{55, 56, NPSEQ_RED}},
    {{0, 14, NPSEQ_RED},{24, 48, NPSEQ_RED},{55, 57, NPSEQ_RED}},
    {{0, 14, NPSEQ_RED},{25, 43, NPSEQ_RED},{55, 58, NPSEQ_RED}},
    {{0, 13, NPSEQ_RED},{27, 33, NPSEQ_RED},{55, 59, NPSEQ_RED}},
    {{0, 10, NPSEQ_RED},{55, 60, NPSEQ_RED}},
    {{0, 6, NPSEQ_RED},{23, 23, NPSEQ_RED},{55, 61, NPSEQ_RED}}, // 20

    {{23, 23, NPSEQ_RED},{55, 61, NPSEQ_RED}},
    {{23, 23, NPSEQ_RED},{55, 61, NPSEQ_RED}},
    {{23, 23, NPSEQ_RED},{55, 61, NPSEQ_RED}},
    {{23, 23, NPSEQ_RED},{55, 61, NPSEQ_RED}},
    {{23, 23, NPSEQ_RED},{54, 61, NPSEQ_RED}},
    {{23, 23, NPSEQ_RED},{54, 61, NPSEQ_RED}},
    {{23, 23, NPSEQ_RED},{54, 60, NPSEQ_RED}},
    {{23, 23, NPSEQ_RED},{54, 60, NPSEQ_RED}},
    {{22, 22, NPSEQ_RED},{27, 27, NPSEQ_RED},{54, 60, NPSEQ_RED}},
    {{22, 22, NPSEQ_RED},{28, 28, NPSEQ_RED},{53, 60, NPSEQ_RED}}, //30

    {{22, 22, NPSEQ_RED},{29, 29, NPSEQ_RED},{53, 60, NPSEQ_RED}},
    {{29, 29, NPSEQ_RED},{53, 60, NPSEQ_RED}},
    {{5, 5, NPSEQ_RED},{29, 29, NPSEQ_RED},{52, 60, NPSEQ_RED}},
    {{5, 5, NPSEQ_RED},{29, 29, NPSEQ_RED},{52, 60, NPSEQ_RED}},
    {{6, 6, NPSEQ_RED},{29, 29, NPSEQ_RED},{51, 60, NPSEQ_RED}},
    {{7, 7, NPSEQ_RED},{28, 28, NPSEQ_RED},{51, 60, NPSEQ_RED}},
    {{8, 9, NPSEQ_RED},{27, 27, NPSEQ_RED},{50, 59, NPSEQ_RED}},
    {{10, 10, NPSEQ_RED},{25, 26, NPSEQ_RED},{49, 59, NPSEQ_RED}},
    {{11, 11, NPSEQ_RED},{24, 24, NPSEQ_RED},{49, 59, NPSEQ_RED},{60, 60, NPSEQ_DARKRED},},
    {{7, 10, NPSEQ_RED},{12, 12, NPSEQ_RED},{23, 26, NPSEQ_RED},{48, 59, NPSEQ_RED},{60, 63, NPSEQ_DARKRED}}, //40

    {{5, 12, NPSEQ_RED},{22, 29, NPSEQ_RED},{47, 59, NPSEQ_RED},{60, 66, NPSEQ_DARKRED}},
    {{3, 32, NPSEQ_RED},{46, 59, NPSEQ_RED},{60, 69, NPSEQ_DARKRED}},
    {{2, 34, NPSEQ_RED},{46, 59, NPSEQ_RED},{60, 72, NPSEQ_DARKRED}},
    {{0, 36, NPSEQ_RED},{45, 59, NPSEQ_RED},{60, 75, NPSEQ_DARKRED}},
    {{0, 38, NPSEQ_RED},{44, 58, NPSEQ_RED},{59, 79, NPSEQ_DARKRED}},
    {{0, 40, NPSEQ_RED},{43, 58, NPSEQ_RED},{59, 81, NPSEQ_DARKRED}},
    {{0, 58, NPSEQ_RED},{59, 83, NPSEQ_DARKRED}},
    {{0, 13, NPSEQ_RED},{21, 58, NPSEQ_RED},{59, 84, NPSEQ_DARKRED}},
    {{0, 10, NPSEQ_RED},{23, 58, NPSEQ_RED},{59, 84, NPSEQ_DARKRED}},
    {{0, 8, NPSEQ_RED},{25, 58, NPSEQ_RED},{59, 85, NPSEQ_DARKRED}}, // 50

    {{36, 57, NPSEQ_RED},{58, 85, NPSEQ_DARKRED}},
    {{36, 57, NPSEQ_RED},{58, 85, NPSEQ_DARKRED}},
    {{35, 57, NPSEQ_RED},{58, 84, NPSEQ_DARKRED}},
    {{34, 57, NPSEQ_RED},{58, 84, NPSEQ_DARKRED}},
    {{0, 1, NPSEQ_RED},{33, 57, NPSEQ_RED},{58, 84, NPSEQ_DARKRED}},
    {{0, 2, NPSEQ_RED},{32, 56, NPSEQ_RED},{57, 83, NPSEQ_DARKRED}},
    {{0, 3, NPSEQ_RED},{12, 22, NPSEQ_RED},{31, 56, NPSEQ_RED},{57, 83, NPSEQ_DARKRED}},
    {{0, 4, NPSEQ_RED},{11, 23, NPSEQ_RED},{30, 56, NPSEQ_RED},{57, 83, NPSEQ_DARKRED}},
    {{0, 5, NPSEQ_RED},{10, 24, NPSEQ_RED},{29, 56, NPSEQ_RED},{57, 83, NPSEQ_DARKRED}},
    {{0, 6, NPSEQ_RED},{9, 55, NPSEQ_RED},{56, 83, NPSEQ_DARKRED}}, // 60

    {{0, 55, NPSEQ_RED},{56, 83, NPSEQ_DARKRED}},
    {{0, 55, NPSEQ_RED},{56, 83, NPSEQ_DARKRED}},
    {{0, 54, NPSEQ_RED},{55, 84, NPSEQ_DARKRED}},
    {{0, 54, NPSEQ_RED},{55, 84, NPSEQ_DARKRED}},
    {{0, 54, NPSEQ_RED},{55, 84, NPSEQ_DARKRED}},
    {{0, 53, NPSEQ_RED},{54, 84, NPSEQ_DARKRED}},
    {{0, 53, NPSEQ_RED},{54, 84, NPSEQ_DARKRED}},
    {{0, 53, NPSEQ_RED},{54, 85, NPSEQ_DARKRED}},
    {{0, 52, NPSEQ_RED},{53, 86, NPSEQ_DARKRED}},
    {{0, 52, NPSEQ_RED},{53, 88, NPSEQ_DARKRED}}, // 70

    {{0, 52, NPSEQ_RED},{53, 89, NPSEQ_DARKRED}},
    {{0, 51, NPSEQ_RED},{52, 90, NPSEQ_DARKRED}},
    {{0, 51, NPSEQ_RED},{52, 90, NPSEQ_DARKRED}},
    {{0, 50, NPSEQ_RED},{51, 89, NPSEQ_DARKRED}},
    {{0, 50, NPSEQ_RED},{51, 88, NPSEQ_DARKRED}},
    {{0, 49, NPSEQ_RED},{50, 87, NPSEQ_DARKRED}},
    {{0, 49, NPSEQ_RED},{50, 86, NPSEQ_DARKRED}},
    {{0, 48, NPSEQ_RED},{49, 85, NPSEQ_DARKRED}},
    {{0, 47, NPSEQ_RED},{48, 84, NPSEQ_DARKRED}},
    {{0, 46, NPSEQ_RED},{47, 81, NPSEQ_DARKRED}}, // 80

    {{0, 45, NPSEQ_RED},{46, 61, NPSEQ_DARKRED}},
    {{0, 45, NPSEQ_RED},{46, 53, NPSEQ_DARKRED}},
    {{0, 44, NPSEQ_RED},{45, 48, NPSEQ_DARKRED}},
    {{0, 42, NPSEQ_RED},{43, 45, NPSEQ_DARKRED}},
    {{0, 40, NPSEQ_RED},{41, 43, NPSEQ_DARKRED}},
    {{0, 38, NPSEQ_RED},{39, 41, NPSEQ_DARKRED}},
    {{0, 2, NPSEQ_DARKRED},{3, 35, NPSEQ_RED},{36, 39, NPSEQ_DARKRED}},
    {{0, 8, NPSEQ_DARKRED},{9, 33, NPSEQ_RED},{34, 36, NPSEQ_DARKRED}},
    {{0, 12, NPSEQ_DARKRED},{13, 29, NPSEQ_RED},{30, 34, NPSEQ_DARKRED}},
    {{0, 32, NPSEQ_DARKRED}}, // 90

    {{0, 31, NPSEQ_DARKRED}},
    {{0, 30, NPSEQ_DARKRED}},
    {{0, 29, NPSEQ_DARKRED}},
    {{0, 29, NPSEQ_DARKRED}},
    {{0, 29, NPSEQ_DARKRED}},
    {{0, 29, NPSEQ_DARKRED}},
    {{0, 28, NPSEQ_DARKRED}},
    {{0, 28, NPSEQ_DARKRED}},
    {{0, 28, NPSEQ_DARKRED}},
    {{0, 28, NPSEQ_DARKRED}} //100
  };

#endif