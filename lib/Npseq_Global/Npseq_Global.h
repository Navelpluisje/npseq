//global.h

#ifndef NP_GLOBAL_H
  #define NP_GLOBAL_H

  #include <constants.h>

  void setupControls(IOExpander &settings);
  void setupEncoders(IOExpander &settings);
  void setupOutputs(IOExpander &settings);

  void setControlOutput(byte &base, byte index, bool value);

#endif
