#include <Npseq_Global.h>
#include <Arduino.h>
#include <constants.h>
#include <Npseq_MCP23S17.h>

void setupControls(IOExpander &settings) {
  //Config MCP => Mirror interrupts, disable sequential mode, enable hardware adressing
  // Disabled Interrupt mirroring. Now works directly. Otherwise have to connect interrupt pin by hand
  expanderWrite(csEnc, IOCON, 0b01101000, settings.address);

  pinMode(settings.interrupt, INPUT_PULLUP);
  //Set PORT A registers
  expanderWrite(csEnc, IODIRA, 0b00001111, settings.address);   // 1 is input
  expanderWrite(csEnc, GPPUA, 0b00000000, settings.address);    // No pullups please
  expanderWrite(csEnc, IOPOLA, 0b11111111, settings.address);   // Inverse Polarity
  expanderWrite(csEnc, INTCONA, 0b00000000, settings.address);  // Interrupt on change
  expanderWrite(csEnc, GPINTENA, 0b00001111, settings.address); // Enable Interrupts

  //Set PORT B registers
  expanderWrite(csEnc, IODIRB, 0b00000011, settings.address);   // 1 is input
  expanderWrite(csEnc, GPPUB, 0b00000000, settings.address);    // No pullups please
  expanderWrite(csEnc, IOPOLB, 0b11111111, settings.address);   // Inverse Polarity
  expanderWrite(csEnc, GPINTENB, 0b00000000, settings.address); // Disable interrrupts

  attachInterrupt(digitalPinToInterrupt(settings.interrupt), settings.callback, FALLING);
}

void setupEncoders(IOExpander &settings) {
  //Config MCP => Mirror interrupts, disable sequential mode, enable hardware adressing
  // Disabled Interrupt mirroring. Now works directly. Otherwise have to connect interrupt pin by hand
  expanderWrite(csEnc, IOCON, 0b01101000, settings.address);

  pinMode(settings.interrupt, INPUT_PULLUP);
  //Set PORT A registers
  expanderWrite(csEnc, IODIRA, 0b11111111, settings.address);   // 1 is input
  expanderWrite(csEnc, GPPUA, 0b00000000, settings.address);    // No pullups please
  expanderWrite(csEnc, IOPOLA, 0b11111111, settings.address);   // Inverse Polarity
  expanderWrite(csEnc, INTCONA, 0b00000000, settings.address);  // Interrupt on change
  expanderWrite(csEnc, GPINTENA, 0b11111111, settings.address); // Enable Interrupts

  //Set PORT B registers
  expanderWrite(csEnc, IODIRB, 0b11111111, settings.address);   // 1 is input
  expanderWrite(csEnc, GPPUB, 0b00000000, settings.address);    // No pullups please
  expanderWrite(csEnc, IOPOLB, 0b11111111, settings.address);   // Inverse Polarity
  expanderWrite(csEnc, GPINTENB, 0b00000000, settings.address); // Disable interrrupts

  attachInterrupt(digitalPinToInterrupt(settings.interrupt), settings.callback, FALLING);
}

void setupOutputs(IOExpander &settings) {
  //Config MCP => Mirror interrupts, disable sequential mode, enable hardware adressing
  // Disabled Interrupt mirroring. Now works directly. Otherwise have to connect interrupt pin by hand
  expanderWrite(csEnc, IOCON, 0b01101000, settings.address);

  pinMode(settings.interrupt, INPUT_PULLUP);
  //Set PORT A registers
  expanderWrite(csEnc, IODIRA, 0b00000000, settings.address);   // 1 is input
  expanderWrite(csEnc, GPPUA, 0b00000000, settings.address);    // No pullups please
  expanderWrite(csEnc, IOPOLA, 0b00000000, settings.address);   // Inverse Polarity
  expanderWrite(csEnc, INTCONA, 0b00000000, settings.address);  // Interrupt on change
  expanderWrite(csEnc, GPINTENA, 0b00000000, settings.address); // Enable Interrupts

  //Set PORT B registers
  expanderWrite(csEnc, IODIRB, 0b00000000, settings.address);   // 1 is input
  expanderWrite(csEnc, GPPUB, 0b00000000, settings.address);    // No pullups please
  expanderWrite(csEnc, IOPOLB, 0b00000000, settings.address);   // Inverse Polarity
  expanderWrite(csEnc, GPINTENB, 0b00000000, settings.address); // Disable interrrupts
}

/**
 * Setting binary value to the given index in the base
 */
void setControlOutput(byte &base, byte index, bool value) {
  if(value) {
    base |= 1 << index;
  } else {
    base &= ~(1 << index);
  }
}

