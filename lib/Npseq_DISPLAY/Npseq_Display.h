//displayFunctions.h

#ifndef NPSEQ_DISPLAY_H
  #define NPSEQ_DISPLAY_H
  #include <string>
  #include <Arduino.h>
  #include <ST7735_t3.h> // Hardware-specific library

  void drawStartScreen(ST7735_t3 tft, String version);
  void drawRotary(ST7735_t3 tft, int x, int y, int r, int value);
  void drawNote(ST7735_t3 tft, int x, int y, int value);
  void drawDefaultScreen(ST7735_t3 tft);

#endif
