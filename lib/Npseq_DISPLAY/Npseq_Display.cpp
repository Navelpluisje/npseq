//logoFunctions.cpp

#include <Npseq_Display.h>
#include <string>
#include <Arduino.h>
#include <Adafruit_GFX.h>    // Core graphics library
#include <ST7735_t3.h> // Hardware-specific library
#include <constants.h>

void setupLogo( ST7735_t3 tft ) {
  tft.fillScreen(ST7735_WHITE);
  tft.setRotation(3);
  delay(100);
  tft.fillRect(0, 100, tft.width(), 28, NPSEQ_DARKRED);
}

void drawImage( ST7735_t3 tft ) {
  for (int line = 0; line < 100; line+=1) {
    for(int part = 0; part < 6; part+=1) {
      // the '-1' prevents an unwanted black line in the most left column
      tft.drawLine(image[line][part][0]-1, line, image[line][part][1]-1, line, image[line][part][2]);
    }
    delay(40);
  }
  // Set the title of the application
  tft.setCursor(40, 50);
  // tft.setFont(&FreeSansBold18pt7b);
  tft.setTextColor(ST7735_BLACK);
  tft.print("npSeq");
  // Reset the font to default
  tft.setFont();
}

void setName( ST7735_t3 tft ) {
  tft.setCursor(4, 111);
  tft.setTextColor(ST7735_WHITE);
  tft.print("By Navelpluisje");
}

void setVersion( ST7735_t3 tft, String version ) {
  tft.setRotation(3);
  tft.setCursor(122, 111);
  tft.setTextColor(ST7735_WHITE);
  tft.print("v" + version + "!");
}

void drawDefaultScreen(ST7735_t3 tft) {
  tft.fillScreen(ST7735_WHITE);
  drawRotary(tft, 10, 100, 8, 0);
  drawRotary(tft, 30, 100, 8, 100);
  drawRotary(tft, 50, 100, 8, 63);
  drawRotary(tft, 70, 100, 8, 32);
  drawRotary(tft, 90, 100, 8, 110);
  drawRotary(tft, 110, 100, 8, 89);
  drawRotary(tft, 130, 100, 8, 23);
  drawRotary(tft, 150, 100, 8, 10);

  drawRotary(tft, 10, 80, 8, 0);
  drawRotary(tft, 30, 80, 8, 80);
  drawRotary(tft, 50, 80, 8, 63);
  drawRotary(tft, 70, 80, 8, 32);
  drawRotary(tft, 90, 80, 8, 110);
  drawRotary(tft, 110, 80, 8, 89);
  drawRotary(tft, 130, 80, 8, 23);
  drawRotary(tft, 150, 80, 8, 10);

  drawNote(tft, 10, 120, 45);
  drawNote(tft, 30, 120, 34);
  drawNote(tft, 50, 120, 74);
  drawNote(tft, 70, 120, 89);
  drawNote(tft, 90, 120, 123);
  drawNote(tft, 110, 120, 45);
  drawNote(tft, 130, 120, 23);
  drawNote(tft, 150, 120, 127);
}

/**
 * Map the value to the corresponding value in degrees
 * @param value The value to map to degrees
 * @return An integer value representing the degrees
 */
int calcDegrees(int value) {
  return map(value, 0, 128, -150, 150);
}

int calcXoffset(int value, int radius, int origin) {
  return origin + sin(calcDegrees(value) * PI / 180) * radius;
}

int calcYoffset(int value, int radius, int origin) {
  return origin + (-1 * cos(calcDegrees(value) * PI / 180) * radius);
}

void drawStartScreen( ST7735_t3 tft, String version ) {
  setupLogo(tft);
  delay(100);
  drawImage(tft);
  setName(tft);
  delay(750);
  setVersion(tft, version);
}

void drawRotary( ST7735_t3 tft, int x, int y, int r, int value ) {
  tft.fillCircle(x, y, r, NPSEQ_GRAY);
  tft.drawCircle(x, y, r, NPSEQ_BLACK);
  tft.drawLine(x, y, calcXoffset(value, r, x), calcYoffset(value, r, y), NPSEQ_BLACK);
}

void drawNote( ST7735_t3 tft, int x, int y, int value ) {
  String note = notesList[value];
  tft.setCursor(x - (6 * note.length() / 2), y - 4);
  tft.setTextColor(NPSEQ_DARKRED);
  tft.print(note);
}
