//Npseq_Menu.h

#ifndef NPSEQ_MENU_H
  #define NPSEQ_MENU_H

  #include <ST7735_t3.h> // Hardware-specific library

  void showMenu(ST7735_t3 &tft, byte activeMenuItem);
  void updateMenu(ST7735_t3 &tft, byte activeMenuItem);

  void showSteps(ST7735_t3 &tft, byte steps);
  void showMIDIPort(ST7735_t3 &tft, byte steps);
  void showAbout(ST7735_t3 &tft);

  void updateValue(ST7735_t3 &tft, byte value);

#endif
