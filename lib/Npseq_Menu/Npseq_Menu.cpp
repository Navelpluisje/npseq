//logoFunctions.cpp

#include <Npseq_Menu.h>
#include <Arduino.h>
#include <Adafruit_GFX.h>    // Core graphics library
#include <ST7735_t3.h> // Hardware-specific library
#include <constants.h>

void setMenuHeader(ST7735_t3 &tft, char* title) {
  tft.fillRect(0, 0, 160, 35, NPSEQ_RED);
  tft.setCursor(10, 10);
  tft.setTextColor(ST7735_WHITE);
  tft.setTextSize(2);
  tft.print(title);
  tft.setTextSize(1);
}

void clearMenu(ST7735_t3 &tft) {
  tft.fillRect(0, 35, 160, 128, ST7735_WHITE);
}

void showMenu(ST7735_t3 &tft, byte activeMenuItem) {
  clearMenu(tft);
  setMenuHeader(tft, seqMenu[0].name);

  tft.setTextColor(NPSEQ_RED);
  for(byte i = 1; i < nbMenuItems; i++) {
    byte yPos = 40 + (i - 1) * 14;
    if (i > 1) {
      tft.drawLine(10, yPos - 4, 150, yPos - 4, NPSEQ_DARKRED);
    }
  }

  updateMenu(tft, activeMenuItem);
}

void updateMenu(ST7735_t3 &tft, byte activeMenuItem) {
  for(byte i = 1; i < nbMenuItems; i++) {
    char* name = seqMenu[i].name;
    byte yPos = 40 + (i - 1) * 14;
    byte xPos = 10;

    tft.fillRect(xPos, yPos, 140, 8, ST7735_WHITE);
    tft.setCursor(xPos, yPos);
    if (i == activeMenuItem) {
      tft.setTextColor(NPSEQ_BLACK);
      tft.print("-");
      tft.setCursor(xPos, yPos);
      tft.print("> ");
      tft.setTextColor(NPSEQ_RED);
    }
    tft.print(name);
  }
}

void showSteps(ST7735_t3 &tft, byte steps) {
  clearMenu(tft);
  setMenuHeader(tft, seqMenu[1].name);

  tft.setTextColor(NPSEQ_BLACK);
  tft.setCursor(10, 45);
  tft.print("Set the sequence length");

  tft.setTextColor(NPSEQ_RED);
  tft.setCursor(10, 80);
  tft.setTextSize(2);
  tft.print("Steps:");
  updateValue(tft, steps);
}

void updateValue(ST7735_t3 &tft, byte value) {
  tft.fillRect(70, 75, 140, 110, ST7735_WHITE);
  tft.setCursor(80, 80);
  tft.setTextSize(3);
  tft.print(value);
  tft.setTextSize(1);
}

void showMIDIPort(ST7735_t3 &tft, byte port) {
  clearMenu(tft);
  setMenuHeader(tft, seqMenu[2].name);

  tft.setTextColor(NPSEQ_BLACK);
  tft.setCursor(10, 45);
  tft.print("Select your MIDIport");
  tft.setCursor(10, 57);
  tft.print("(0 = all)");

  tft.setTextColor(NPSEQ_RED);
  tft.setCursor(10, 80);
  tft.setTextSize(2);
  tft.print("Port:");
  updateValue(tft, port);
}

void showAbout(ST7735_t3 &tft) {
  clearMenu(tft);
  setMenuHeader(tft, seqMenu[3].name);

  tft.setTextColor(NPSEQ_BLACK);
  tft.setCursor(10, 45);
  tft.print("Je suis NAVELPLUISJE");

  tft.setCursor(11, 110);
  tft.print("-");
  tft.setCursor(10, 110);
  tft.print("< ");
  tft.setTextColor(NPSEQ_RED);
  tft.print("Click to return");
}


