#include <Npseq_Data.h>
#include <Arduino.h>
#include <constants.h>

SeqData::SeqData(byte _tempo, byte _steps, byte _port) {
  setTempo(_tempo);
  setSteps(_steps);
  setPort(_port);
}

void SeqData::incrementCurrentStep() {
  currentStep += 1;
}
void SeqData::decrementCurrentStep() {
  currentStep -= 1;
}
void SeqData::resetCurrentStep() {
  currentStep = 0;
}

void SeqData::toggleReverse() {
  reverse = !reverse;
}

void SeqData::setTempo(byte value) {
  tempo = value;
}
void SeqData::incrementTempo(byte value) {
  if (tempo + 1 < SEQ_MAX_TEMPO) {
    tempo += 1;
  }
}
void SeqData::decrementTempo(byte value) {
  if (tempo > SEQ_MIN_TEMPO) {
    tempo -= 1;
  }
}

void SeqData::setPort(byte value) {
  port = value;
}
void SeqData::incrementPort(byte value) {
  if (port < SEQ_MAX_PORT) {
    port += 1;
  }
}
void SeqData::decrementPort(byte value) {
  if (port > SEQ_MIN_PORT) {
    port -= 1;
  }
}

void SeqData::setSteps(byte value) {
  steps = value;
}
void SeqData::incrementSteps(byte value) {
  if (steps < SEQ_MAX_STEPS) {
    steps += 1;
  }
}
void SeqData::decrementSteps(byte value) {
  if (steps > SEQ_MIN_STEPS) {
    steps -= 1;
  }
}

void SeqData::toggleNoteOn(byte step) {
  noteOn[step] = !noteOn[step];
}
bool SeqData::getNoteOn(byte step) {
  return noteOn[step];
}

void SeqData::setNote(byte step, byte note) {
  notes[step] = note;
}
byte SeqData::getNote(byte step) {
  return notes[step];
}

void SeqData::setVelocity(byte step, byte velocity) {
  velocities[step] = velocity;
}
byte SeqData::getVelocity(byte step) {
  return velocities[step];
}
