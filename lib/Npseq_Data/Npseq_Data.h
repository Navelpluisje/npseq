
#ifndef NPSEQ_DATA_H
  #define NPSEQ_DATA_H

#include <Arduino.h>
#include <constants.h>

class SeqData {
  private:
    unsigned int tempo;
    unsigned int port;
    byte steps;
    byte currentStep = 0;
    bool reverse = false;
    bool trigger[SEQ_MAX_STEPS];
    bool noteOn[SEQ_MAX_STEPS];
    int8_t notes[SEQ_MAX_STEPS];
    int8_t velocities[SEQ_MAX_STEPS];


  public:
    SeqData(byte _tempo, byte _steps, byte _port);

    void incrementCurrentStep(void);
    void decrementCurrentStep(void);
    void resetCurrentStep(void);

    void toggleReverse(void);

    void setTempo(byte value);
    void incrementTempo(byte value);
    void decrementTempo(byte value);

    void setPort(byte value);
    void incrementPort(byte value);
    void decrementPort(byte value);

    void setSteps(byte value);
    void incrementSteps(byte value);
    void decrementSteps(byte value);

    void toggleNoteOn(byte step);
    bool getNoteOn(byte step);
    void setNote(byte step, byte note);
    byte getNote(byte step);
    void setVelocity(byte step, byte velocity);
    byte getVelocity(byte step);

    // The Getters
    unsigned int getTempo() { return tempo; }
    byte getSteps() { return steps; }
    byte getPort() { return port; }
};
#endif
