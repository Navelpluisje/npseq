#include <stdint.h>
#include <Npseq_MCP23S17.h>
#include <constants.h>
#include <Arduino.h>
#include <SPI.h>

//Write to expander
void expanderWrite(
  const byte cs,
  const byte reg,
  const byte data,
  const byte address
) {
  digitalWrite( cs, LOW );
  SPI.transfer( address << 1 );  // note this is write mode
  SPI.transfer( reg );
  SPI.transfer( data );
  digitalWrite( cs, HIGH );
}

//Read from expander
byte expanderRead(
  const byte cs,
  const byte reg,
  const byte address
) {
  byte data = 0;
  digitalWrite( cs, LOW );
  SPI.transfer( (address << 1) | 1 );  // note this is read mode
  SPI.transfer( reg );
  data = SPI.transfer( 0 );
  digitalWrite( cs, HIGH );
  return data;
}
