# Da Fritzing Files

## ¿¿¿Da what???
These files are for creating schematics and pcb layouts. You need to have the [Fritzing](http://fritzing.org) tool installed, which can be downloaded from their site.

## Exporting PCB's
When exporting stuf for the pcb's you can use the folders corresponding to the filenames. They are added to the gitignore file, so will not be committed.



