# Pin Layout

## Global Pins
* `mosi` = 11 _Master Out Slave In_
* `miso` = 12 _Master In Slave Out_
* `sclk` = 13 _SPI Clock_


## TFT Pins
* `rstTFT` = 8  _Reset Pin for TFT Display
* `dcTFT`  = 9  _but certain pairs must NOT be used: 2+10, 6+9, 20+23, 21+22
* `csTFT`  = 10 _Chip/Slave Select for the TFT display_


## Expander Pins
* `rstEnc`  = 6  _Reset pin for the I/O encoder expanders
* `csEnc`   = 7  _Chip/Slave Select for the I/O encoder expanders_
* `intEnc0` = 18 _ Interrupt pins_
* `intEnc1` = 19
* `intEnc2` = 20
* `intEnc3` = 21
* `intEnc4` = 22