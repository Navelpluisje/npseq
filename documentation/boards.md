# Boards

## I/O Expander board

![Expander-board](assets/board-io-expander.png)

* `J1`: Connecting to `J1` of the Encoder board
* `J2`: Connecting to `J2` of the Encoder board
* `J3`: Connectiong to Teensy
* `J4 & J5`: Use jumper tpo set the right interupt
* `R1-R3`: Setthe hardware address of the MCP23S17

## Encoder board

![Expander-board](assets/board-encoders-1.png)

![Expander-board](assets/board-encoders-2.png)

* `J1`: Connecting to `J1` of the I/O Expander board
* `J2`: Connecting to `J2` of the I/O Expander board
* `R1-R32`: All 10K resistors
* `C1-C16`: All 10n capacitors
