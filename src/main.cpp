// This Teensy3 native optimized version requires specific pins

#include <functional>
#include <vector>
#include <Arduino.h>
#include <ST7735_t3.h> // Hardware-specific library
#include <SPI.h>

// From da Libs
#include <constants.h>
#include <Npseq_Global.h>
#include <Npseq_Data.h>
#include <Npseq_Display.h>
#include <Npseq_Menu.h>
#include <Npseq_MCP23S17.h>

ST7735_t3 tft = ST7735_t3(csTFT, dcTFT, mosiTFT, clkTFT, rstTFT);
SeqData seqData(120, 16, 16);

char* version = getenv("VERSION");

IOExpander expSettings[] = {
  {0x20, 18, false, 0, NPSEQ_CONTROL_EXPANDER, [](){expSettings[0].interrupted = true;}},
  {0x21, 19, false, 0, NPSEQ_OUTPUT_EXPANDER, [](){expSettings[1].interrupted = true;}},
  // {0x22, 20, false},
  // {0x23, 21, false},
  // {0x24, 22, false},
};
const int nbExpanders = sizeof(expSettings) / sizeof(*expSettings);

void setupExpanders() {
  for (byte i = 0; i < nbExpanders; i++) {
    switch (expSettings[0].type) {
      case NPSEQ_CONTROL_EXPANDER:
        setupControls(expSettings[i]);
        break;
      case NPSEQ_ENC_EXPANDER:
        setupEncoders(expSettings[i]);
        break;
      case NPSEQ_OUTPUT_EXPANDER:
        setupOutputs(expSettings[i]);
        break;
    }
  }
}

/*
 * Menu stuff over here
 */
bool seqRunning = false;
bool menuActive = false;
byte activeMenuItem = 0;
byte selectedMenuItem = 0;

unsigned long prevTime = millis();
int step = 0;
int value = 0;

void setupTFT() {
  // Initialize the display
  tft.initR(INITR_BLACKTAB);
  tft.setRotation(3);
  tft.fillScreen(ST7735_WHITE);

  drawStartScreen(tft, version);
  drawDefaultScreen(tft);
}

void setup(void) {
  pinMode(mosiTFT, OUTPUT);
  pinMode(clkTFT, OUTPUT);
  pinMode(csTFT, OUTPUT);
  digitalWrite(csTFT, HIGH);
  pinMode(csEnc, OUTPUT);
  digitalWrite(csEnc, HIGH);

  SPI.begin();

  setupTFT();
  setupExpanders();
  expanderWrite(csEnc, GPIOA, 0b01010101, expSettings[1].address);

}

void closeMenu() {
  menuActive = 0;
  activeMenuItem = 0;
  selectedMenuItem = 0;
  drawDefaultScreen(tft);
}

void incrementMenuItem() {
  if (activeMenuItem + 1 < nbMenuItems) {
    activeMenuItem += 1;
    updateMenu(tft, activeMenuItem);
  }
}

void decrementMenuItem() {
  if (activeMenuItem > 0) {
    activeMenuItem -= 1;
    updateMenu(tft, activeMenuItem);
  }
}

byte looper[] = {
  0b10000000,
  0b01000000,
  0b00100000,
  0b00010000,
  0b00001000,
  0b00000100,
  0b00000010,
  0b00000001,
};

void loop() {
  unsigned long newTime = millis();

  if (newTime - prevTime >= 500) {
    expanderWrite(csEnc, GPIOB, looper[step], expSettings[1].address);
    step += 1;
    if (step == 8) { step = 0; }
    prevTime = newTime;
  }

  if (expSettings[0].interrupted) {
    // Reset the interrupt
    expSettings[0].interrupted = false;
    // Read the A and B-ports
    byte portA = expanderRead(csEnc, GPIOA, expSettings[0].address);
    byte portB = expanderRead(csEnc, GPIOB, expSettings[0].address);

    byte mask = 0;
    byte gpio = 0; // The curent IO triggered
    byte aCurrent; // Current value of the corresponding IO
    byte aLast;    // Previous value of the corresponding IO

    // Check which port caused the interrupt
    for(gpio = 0; gpio < 4; gpio++) {
      mask = 1 << gpio;
      aLast = expSettings[0].portALast & mask;
      aCurrent = portA & mask;

      // If we found the port, break the loop
      if(aLast ^ aCurrent) {
        break;
      }
    }

    //If the current value is true and
    byte portAOut = portA;
    if(aCurrent) {
      if(!aLast) {
        expSettings[0].portALast = portA;
        byte bState = mask & portB;

        switch (gpio) {
          case 0:
            // Handle the control encoder.
            // Depends on the menu-state what it does
            if (menuActive) {
              if (selectedMenuItem == 0) {
                if (bState) { // Clockwise
                  incrementMenuItem();
                } else {
                  decrementMenuItem();
                }
              } else {
                switch (selectedMenuItem) {
                  case 1:
                    if (bState) { // Clockwise
                     seqData.incrementSteps(1);
                    } else {
                      seqData.decrementSteps(1);
                    }
                    updateValue(tft, seqData.getSteps());
                    break;
                  case 2:
                    if (bState) { // Clockwise
                      seqData.incrementPort(1);
                    } else {
                      seqData.decrementPort(1);
                    }
                    updateValue(tft, seqData.getPort());
                    break;
                }
              }
            } else {
              if (bState) { // Clockwise
                seqData.incrementTempo(5);
              } else {
                seqData.decrementTempo(5);
              }
            }
            break;
          case 1:
            break;
          case 2:
            if (menuActive) {
              if (selectedMenuItem == 0) {
                selectedMenuItem = activeMenuItem;
                switch (selectedMenuItem) {
                  case 1:
                    showSteps(tft, seqData.getSteps());
                    break;
                  case 2:
                    showMIDIPort(tft, seqData.getPort());
                    break;
                  case 3:
                    showAbout(tft);
                    break;
                  case 4:
                    closeMenu();
                    break;
                }
              } else {
                selectedMenuItem = 0;
                showMenu(tft, activeMenuItem);
              }
            }
            break;
          case 3:
            menuActive = !menuActive;
            if (menuActive) {
              showMenu(tft, activeMenuItem);
            } else {
              closeMenu();
            }
            break;
        }

        setControlOutput(portAOut, 7, menuActive);
        setControlOutput(portAOut, 6, seqRunning);

      }
      expanderWrite(csEnc, GPIOA, portAOut, expSettings[0].address);
    } else {
      if(aLast) {
        expSettings[0].portALast = portA;
      }
    }
  }

  // for (int expander = 0; expander < nbExpanders; expander++) {
  //   if (expSettings[expander].interrupted) {
  //     Serial.print("Expander: ");
  //     Serial.println(expander);
  //     expSettings[expander].interrupted = false;
  //     byte portA = expanderRead(csEnc, GPIOA, expSettings[expander].address);
  //     byte portB = expanderRead(csEnc, GPIOB, expSettings[expander].address);

  //     byte mask = 0;
  //     byte aLast;
  //     byte aCurrent;

  //     for(byte j = 0; j < 8; j++) {
  //         mask = 1 << j;
  //         aLast = expSettings[expander].portALast & mask;
  //         aCurrent = portA & mask;

  //         if(aLast ^ aCurrent) {
  //           break;
  //         }
  //     }

  //     //Then determine the direction of rotation if its the raising edge of the square wave
  //     if(aCurrent) {
  //       if(!aLast) {
  //         expSettings[expander].portALast = portA;
  //         byte bState = mask & portB;

  //         if(bState) { // Clockwise
  //           Serial.println("Clockwise");
  //           if (expander == 0) {
  //             seqData.tempo = seqData.tempo + 5;
  //           }
  //           // counters[j][i]++;
  //         } else { //Counter-Clockwise
  //           Serial.println("Counter Clockwise");
  //           // counters[j][i]--;;
  //           if (expander == 0) {
  //             seqData.tempo = seqData.tempo - 5;
  //           }
  //         }
  //         drawRotary(tft, 10, 100, 8, seqData.tempo);
  //       }
  //     } else {
  //       if(aLast) {
  //         expSettings[expander].portALast = portA;
  //       }
  //     }
  //   }
  // }
}
